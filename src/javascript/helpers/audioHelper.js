export function audioControl(option, filepath) {
    if (!document.getElementById("soundtrack")) {
        const sound = document.createElement('audio');
        sound.loop = true;
        sound.id = "soundtrack";
        sound.src = filepath;
        document.body.appendChild(sound);
        sound.play();

        return;
    }
    
    const sound = document.getElementById('soundtrack');

    if (option === 'play') {
        sound.pause();
        sound.src = filepath;
        sound.play();
    }

    else if (option === 'stop') {
        sound.pause();
    }
}