export function createFighterTable(fighter) {

  const capitalizeFirstCharacter = ((string) => {
    const capitalizedChar = string.charAt(0).toUpperCase();
    return capitalizedChar + string.slice(1);
  })

  const tbl = document.createElement('table');
  tbl.style.width = '100%';
  const thead = document.createElement('thead');
  const tr = document.createElement('tr');
  const thName = document.createElement('th');
  thName.setAttribute('colspan', '2');
  thName.innerText = fighter.name;
  tr.appendChild(thName);
  thead.appendChild(tr);
  const tbdy = document.createElement('tbody');
  for (let prop in fighter) {
    if (prop != "_id" && prop != "source" && prop != "name") {
      const tr = document.createElement('tr');
      const stat = document.createElement('td');
      stat.style.paddingLeft = "20px";
      stat.innerText = capitalizeFirstCharacter(prop);
      const statValue = document.createElement('td');
      statValue.innerText = fighter[prop];
      tr.appendChild(stat);
      tr.appendChild(statValue);
      tbdy.appendChild(tr);
    }
  }
  tbl.appendChild(thead);
  tbl.appendChild(tbdy);

  return tbl;
}  