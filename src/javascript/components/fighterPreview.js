import { createElement } from '../helpers/domHelper';
import { createFighterTable } from  '../helpers/tableCreator';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const fighterTable = createFighterTable(fighter);
    fighterElement.appendChild(fighterTable);
    const fighterImg = createFighterImage(fighter)
    fighterImg.src = fighter.source;

    if (position == "right") {
      fighterImg.style.transform = "scale(-1, 1)";
    }

    fighterElement.append(fighterImg);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name, 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
