import { controls } from '../../constants/controls';
import { audioControl } from '../helpers/audioHelper';

export async function fight(FighterOne, FighterTwo) {
  return new Promise((resolve) => {
    audioControl('play', '../../../resources/soundtrack/Street Fighter 2V - Fight Theme.mp3');
    const firstFighter = Object.assign({}, FighterOne);
    const secondFighter = Object.assign({}, FighterTwo);

    const pressedKeysSet = new Set();
    const initialCoolDown = new Date() - 10001;

    firstFighter.coolDown = initialCoolDown;
    secondFighter.coolDown = initialCoolDown;

    firstFighter.healthBar = document.getElementById('left-fighter-indicator');
    secondFighter.healthBar = document.getElementById('right-fighter-indicator');

    firstFighter.currentHP = firstFighter.health;
    secondFighter.currentHP = secondFighter.health;

    const keyDown = function keyDownEventHandler(event) {
      pressedKeysSet.add(event.code);

      if (
        event.code === controls.PlayerOneAttack &&
        !pressedKeysSet.has(controls.PlayerOneBlock) &&
        !pressedKeysSet.has(controls.PlayerTwoBlock) &&
        !event.repeat
      ) {
        changeHealthDefault(firstFighter, secondFighter);
      }

      if (
        event.code === controls.PlayerTwoAttack &&
        !pressedKeysSet.has(controls.PlayerTwoBlock) &&
        !pressedKeysSet.has(controls.PlayerOneBlock) &&
        !event.repeat
      ) {
        changeHealthDefault(secondFighter, firstFighter);
      }

      if (controls.PlayerOneCriticalHitCombination.every((key) => pressedKeysSet.has(key))) {
        changeHealthCrit(firstFighter, secondFighter);
      }

      if (controls.PlayerTwoCriticalHitCombination.every((key) => pressedKeysSet.has(key))) {
        changeHealthCrit(secondFighter, firstFighter);
      }

      if (firstFighter.currentHP <= 0) getWinner(secondFighter);
      if (secondFighter.currentHP <= 0) getWinner(firstFighter);

    }

    const keyUp = (event) => pressedKeysSet.delete(event.code);

    document.addEventListener('keydown', keyDown);
    document.addEventListener('keyup', keyUp);

    const getWinner = (winner) => {
      document.removeEventListener('keydown', keyDown);
      document.removeEventListener('keyup', keyUp);
      resolve(winner);
    };
  });
}

export function changeHealthCrit(attacker, defender) {
  const currentDate = new Date;
  
  if (currentDate - attacker.coolDown > 10000) {
    defender.currentHP -= attacker.attack * 2;
    attacker.coolDown = currentDate;
    changeHealthBar(defender);
  }
}

export function changeHealthDefault(attacker, defender) {
  defender.currentHP -= getDamage(attacker, defender);
  changeHealthBar(defender);
}

export function changeHealthBar(defender) {
  const audio = new Audio('../../../resources/soundtrack/Kick - Sound Effect.mp3');
  audio.play();
  defender.healthBar.style.width = defender.currentHP > 0
    ? (100 * (defender.currentHP / defender.health)).toFixed(0) + '%'
    : 0; 
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  const hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = 1 + Math.random();
  const dodgePower = fighter.defense * dodgeChance;
  return dodgePower;
}
