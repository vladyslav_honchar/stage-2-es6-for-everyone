import { createFighterImage } from '../fighterPreview';
import { showModal } from './modal';
import { audioControl } from '../../helpers/audioHelper';

export function showWinnerModal(fighter) {
  audioControl('stop', null);
  new Audio('../../../../resources/soundtrack/Street Fighter II-You Win Perfect.mp3').play();
  const fighterImage = createFighterImage(fighter);
  const modal = {
    title: `${fighter.name} win`.toUpperCase(),
    bodyElement: fighterImage,
  };
  showModal(modal);
}
